package querybuilder

import (
	"fmt"
	"strings"
)

type QueryBuilder struct {
	selects []string
	table   string
	limit   int
	where   []string
	whereIn []string
	order   []string
	params  []interface{}
}

func NewQueryBuilder(table string) *QueryBuilder {
	return &QueryBuilder{
		table: table,
	}
}

func (qb *QueryBuilder) AddSelect(sql string) {
	qb.selects = append(qb.selects, sql)
}

func (qb *QueryBuilder) AddWhere(sql string, params []interface{}) {
	qb.where = append(qb.where, sql)
	qb.params = append(qb.params, params...)
}

func (qb *QueryBuilder) AddWhereIn(column string, in []interface{}) {
	inSql := ""
	for _, v := range in {
		inSql += "?,"
		qb.params = append(qb.params, v)
	}

	inSql = strings.TrimSuffix(inSql, ",")
	inSql = column + " IN (" + inSql + ")"
	qb.where = append(qb.where, inSql)
}

func (qb *QueryBuilder) Build() string {
	return fmt.Sprintf("SELECT %s FROM %s WHERE %s", strings.Join(qb.selects, ","), qb.table, strings.Join(qb.where, " AND "))
}

func (qb *QueryBuilder) GetParams() []interface{} {
	return qb.params
}

func (qb *QueryBuilder) SetLimit(l int) {
	qb.limit = l
}
